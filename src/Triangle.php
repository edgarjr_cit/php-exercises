<?php

/**
 * Find a type of a triangle based on a given side length.
 *
 * @inheritDoc
 */
class Triangle {

  //quantidade de lados de tamanhos diferentes
  const EQUILATERAL = 1;
  const ISOSCELES   = 2;
  const SCALENE     = 3;

  /**
   * Return a type of a triangle.
   *
   * @param array $numbers
   *   Receive the values of the length of the sides of the triangle.
   *
   * @return string
   *   Return the type of the triangle:
   *    scalene, equilateral or isosceles based on the lengths of the sides.
   */
  public static function triangleType(array $numbers): String {
    // Place your code here:
    $diferentNumbers = count(array_unique($numbers));

    switch($diferentNumbers) {
      case Triangle::EQUILATERAL:
        return 'Equilateral';
        break;
      case Triangle::ISOSCELES:
        return 'Isosceles'; 
        break;
      case Triangle::SCALENE:
        return 'Scalene';
        break;
    }


  }

}
