<?php

/**
 * Return a day of the week of a given integer.
 *
 * @inheritDoc
 */
class DayOfWeek {

  /**
   * Return the day of the week.
   *
   * @param int $number
   *   Receive a integer.
   *
   * @return string
   *   Return a day of the week.
   */
  public static function whatDayOfWeek($number): string {
    // Place your code here:
    $daysOfWeek = DayOfWeek::getArrayOfDays();

    if (array_key_exists($number,$daysOfWeek)) {
      return $daysOfWeek[$number];
    }
    return 'An invalid value was entered.';
  }

  /**
   * Return the days of the week in an array
   *
   * @return array
   */
  private function getArrayOfDays() : array
  {
    return array(
      '1' => 'Sunday',
      '2' => 'Monday',
      '3' => 'Tuesday',
      '4' => 'Wednesday',
      '5' => 'Thursday',
      '6' => 'Friday',
      '7' => 'Saturday'
    );
  }

}
