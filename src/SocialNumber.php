<?php

use PhpParser\Node\Expr\Cast\Bool_;

/**
 * Validate the social number.
 *
 * @inheritDoc
 */
class SocialNumber {

  /**
   * Return boolean if the social number is valid or not.
   *
   * @param string $socialNumber
   *   Receive a string with the social number do be validated.
   *
   * @return bool
   *   Return a bool with the validation
   */
  public static function validateSocialNumber(string $socialNumber): bool {
    // Place your code here:
    $socialNumberArray = str_split($socialNumber);
    $firtDigitValidationNumbers = array_slice($socialNumberArray, 0,-2);
    $secondDigitValidationNumbers  = array_slice($socialNumberArray, 0,-1);
    $lastTwoDigits  = array_slice($socialNumberArray, -2);

    if(!SocialNumber::areAllNumbersEqual($socialNumberArray)){
      if(SocialNumber::firstDigitValidation($firtDigitValidationNumbers) == $lastTwoDigits[0]) {
        if(SocialNumber::secondDigitValidation($secondDigitValidationNumbers) == $lastTwoDigits[1]) {
          return TRUE;
        }
        return FALSE;
      }
      return FALSE;
    }
    return FALSE;

  }

  private function firstDigitValidation(array $numbers) : int
  {
    $max = 10;
    return SocialNumber::digitValidation($numbers, $max);
  }

  private function digitValidation(array $numbers, int $decrementedValue): int
  {
    $total = SocialNumber::multipleArrayItemByDecrementableValue($numbers, $decrementedValue);
    $rest = (($total*10)%11);
    if($rest == 10){
      $rest = 0;
    }
    return $rest;
  }

  private function multipleArrayItemByDecrementableValue(array $numbers, int $decrementedValue): int
  {
    $total = 0;
    foreach($numbers as $number) {
      $total = $total + ($number * $decrementedValue);
      $decrementedValue--;
    }

    return $total;
  }

  private function secondDigitValidation(array $numbers) : int
  {
    $max = 11;
    return SocialNumber::digitValidation($numbers, $max);
  }

  private function areAllNumbersEqual(array $numbers) : bool
  {
    if(count(array_unique($numbers)) == 1) {
      return true;
    }
    return false;
  }

}
