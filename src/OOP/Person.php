<?php

/**
 *
 */
class Person {

  // Place your code here:
  private $name;

  private $gender;

  private $age;

  /**
   * @param string $name
   * @param integer $age
   * @param string $gender
   */
  public function __construct(string $name, int $age, string $gender) {
   // Place your code here:
   $this->name   = $name;
   $this->age    = $age;
   $this->gender = $gender;

  }

  /**
   * @return string
   */
  public function getName() {
    // Place your code here:
    return $this->name;
  }

  /**
   * @return integer
   */
  public function getAge() {
    // Place your code here:
    return $this->age;
  }

  /**
   * @return string
   */
  public function getGender() {
    // Place your code here:
    return $this->gender;
  }

  /**
   * @param string $name
   */
  public function setName(string $name) {
    // Place your code here:
    $this->name = $name;
  }

  /**
   * @param integer $age
   */
  public function setAge(int $age) {
    // Place your code here:
    $this->age = $age;
  }

  /**
   * @param string $gender
   */
  public function setGender(string $gender) {
    // Place your code here:
    $this->gender = $gender;
  }

  /**
   *
   */
  public function birthday() {
    $birthday = $this->age + 1;
    return $birthday;
  }

}
