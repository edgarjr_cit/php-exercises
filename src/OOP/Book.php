<?php

require_once 'Person.php';
require_once 'PublicationInterface.php';

/**
 *
 */
class Book implements PublicationInterface {

  private $tilte;

  private $author;

  private $totalPages;

  private $page;

  private $open;

  private $reader;

  /**
   * @param string $title
   * @param string $author
   * @param string $totalPages
   * @param string $reader
   */
  public function __construct(string $title, string $author, int $totalPages, string $reader) {

    $this->tilte      = $title;
    $this-> author    = $author;
    $this->totalPages = $totalPages;
    $this->reader     = $reader;
  }

  /**
   *
   */
  public function details() {

  }

  /**
   * @return string
   */
  public function getTitle() {
    return $this->tilte;
  }

  /**
   * @return string
   */
  public function getAuthor() {
    return $this->author;
  }

  /**
   * @return integer
   */
  public function getTotalPages() {
   return $this->totalPages;
  }

  /**
   * @return integer
   */
  public function getPage() {
    return $this->page;
  }

  /**
   * @return string
   */
  public function getReader() {
    return $this->reader;
  }

  /**
   * @param string $author
   */
  public function setAuthor(string $author) {
    $this->author = $author;
  }

  /**
   * @param string $title
   */
  public function setTitle(string $title) {
    $this->tilte = $title;
  }

  /**
   * @param integer $totalPages
   */
  public function setTotalPages(int $totalPages) {
    $this->totalPages = $totalPages;
  }

  /**
   * @param integer $page
   */
  public function setPage(int $page) {
    $this->page = $page;
  }

  /**
   * @return bool 
   */
  public function open() {
    $this->open = true;
    return $this->open;
  }

  /**
   * @return bool
   */
  public function close() {
    $this->open = false;
    return $this->open;
  }

  /**
   * @return integer
   */
  public function browse($page) {
    if ($page < 0 || $page > $this->totalPages) {
      return 0;
    }
    return $page;
  }

  /**
   * @return string|integer
   */
  public function jumpForward() {
    if ($this->page == 300) {
      return "You reached the end of the book!";
    }
    return ++$this->page;

  }

  /**
   * @return string|integer
   */
  public function jumpBackWard() {
    if ($this->page == 1) {
      return "You are in the first page of the book!";
    }
    return --$this->page;
  }

}
